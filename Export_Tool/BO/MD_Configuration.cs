﻿using MD.XML;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MD.Configuration
{

    static class EXP_TOOL_Configuration
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        private static readonly string _data = DateTime.Now.ToString("yyyyMMdd_HHmmss");

        /// <summary>
        /// Connessione al Database
        /// </summary>
        static public string EXPORT_TOOL_CONNECTION_DATABASE
        {
            get => System.Configuration.ConfigurationManager.AppSettings["EXPORT_TOOL_CONNECTION_DATABASE"].ToString();
        }

        /// <summary>
        /// Path XML di configurazione
        /// </summary>
        static public string EXPORT_TOOL_PATH_XML
        {
            get => System.Configuration.ConfigurationManager.AppSettings["EXPORT_TOOL_PATH_XML"].ToString();
        }

                /// <summary>
        /// Path LOG di errore
        /// </summary>        
        static public string EXPORT_TOOL_LOG_ERROR
        {
            get => System.Configuration.ConfigurationManager.AppSettings["EXPORT_TOOL_LOG_ERROR"].ToString();
        }        

        static public string EXPORT_TOOL_XML_SITE
        {
            get => System.Configuration.ConfigurationManager.AppSettings["EXPORT_TOOL_XML_SITE"].ToString();
        }


        /// <summary>
        /// Stampo Il contenuto dei parametri settati
        /// </summary>
        static public void LogParameterAttribute()
        {
            try
            {
                log.Info($"Path EXPORT_TOOL_PATH_XML : {EXP_TOOL_Configuration.EXPORT_TOOL_PATH_XML.ToString()}");
                log.Info($"Site EXPORT_TOOL_XML_SITE : {EXP_TOOL_Configuration.EXPORT_TOOL_XML_SITE.ToString()}");              
                log.Info($"Path Log Error : {EXP_TOOL_Configuration.EXPORT_TOOL_LOG_ERROR.ToString()}");
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Effettuo la verifica se le cartelle Principali sono state Create
        /// </summary>
        static public void VerifyPrincipalFolder()
        {
            try
            {                
                if (!File.Exists(EXP_TOOL_Configuration.EXPORT_TOOL_PATH_XML)) { throw new IOException($"XML di configurazione per la query XML non trovato al path {EXP_TOOL_Configuration.EXPORT_TOOL_PATH_XML}"); }
                if (!Directory.Exists(EXP_TOOL_Configuration.EXPORT_TOOL_LOG_ERROR)) { Directory.CreateDirectory(EXP_TOOL_Configuration.EXPORT_TOOL_LOG_ERROR); }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        static public void VerifyPrincipalFolderToXML(MD_Struttura _MD_Struttura)
        {
            try
            {
                if (Directory.Exists(_MD_Struttura.STORAGE_AREA))
                {
                    DirectoryInfo _dirOutput = new DirectoryInfo(_MD_Struttura.STORAGE_AREA);

                    List<DirectoryInfo> _folderEmpty = _dirOutput.GetDirectories().Where(x => x.GetFiles().Length == 0 && x.GetDirectories().Length == 0).ToList();
                    foreach (var _folder in _folderEmpty) { _folder.Delete(); }
                }
                //concateno la stringa della data per permettere di avere tutte le informazioni corrette
                _MD_Struttura.STORAGE_AREA = Path.Combine(_MD_Struttura.STORAGE_AREA, _data);
                if (!Directory.Exists(_MD_Struttura.STORAGE_AREA)) { Directory.CreateDirectory(_MD_Struttura.STORAGE_AREA); }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
