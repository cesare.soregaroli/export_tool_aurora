﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using MD.Configuration;

namespace MD.XML
{
    class MD_XML
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        public List<MD_Struttura> Extract_SqlToXml()
        {
            //Memorizzo le varie strutture dove poi poter verificare le varie configurazioni
            List<MD_Struttura> _List_MD_Struttura = new List<MD_Struttura>() { };

            MD_Struttura _MD_Struttura = null;
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(EXP_TOOL_Configuration.EXPORT_TOOL_PATH_XML);

                //Display all the book titles.
                XmlNodeList elemList = doc.SelectNodes($"/QUERY/ENVIRONMENT[@SITE='{EXP_TOOL_Configuration.EXPORT_TOOL_XML_SITE}']");

                //Estrare L'ambiente di Riferimento
                if (elemList != null && elemList.Count > 0)
                {
                    //Questo ciclo permette di avere a disposizione il nodo da poter eseguite UAT - PRD
                    foreach (XmlNode _elem in elemList)
                    {
                        //In questo momento avrai due tipologie <RELATIONSHIP TYPE="STANDALONE"> o <RELATIONSHIP TYPE="NOT_STANDALONE">
                        foreach (XmlNode _xmlchild in _elem)
                        {

                            _MD_Struttura = new MD_Struttura();
                            _MD_Struttura.RELATIONSHIP = _xmlchild.Attributes["TYPE"].InnerText;
                            _MD_Struttura.STORAGE_AREA_INTERNAL_FOLDER = 1;
                            foreach (XmlNode _xmlchild2 in _xmlchild)
                            {
                                if (_xmlchild2.Name.Equals("ENABLING"))
                                {
                                    _MD_Struttura.ENABLING = Convert.ToBoolean(_xmlchild2.InnerText);
                                }
                                else if (_xmlchild2.Name.Equals("ENABLING_UPDATE"))
                                {
                                    _MD_Struttura.ENABLING_UPDATE = Convert.ToBoolean(_xmlchild2.InnerText);
                                }
                                else if (_xmlchild2.Name.Equals("STORAGE_AREA"))
                                {
                                    _MD_Struttura.STORAGE_AREA = _xmlchild2.InnerText;
                                }
                                else if (_xmlchild2.Name.Equals("ITEM_QUERY"))
                                {
                                    _MD_Struttura.ITEM_QUERY = _xmlchild2.InnerText;
                                }
                                else if (_xmlchild2.Name.Equals("INTEGRAZIONE_FOLDER"))
                                {
                                    _MD_Struttura.INTEGRAZIONE_FOLDER = _xmlchild2.InnerText;
                                }
                                else if (_xmlchild2.Name.Equals("MAX_FILES_IN_FOLDER"))
                                {
                                    if (!string.IsNullOrEmpty(_xmlchild2.InnerText))
                                    {
                                        _MD_Struttura.MAX_FILES_IN_FOLDER = Convert.ToInt32(_xmlchild2.InnerText);
                                    }
                                    else { _MD_Struttura.MAX_FILES_IN_FOLDER = 0; }
                                }
                            }
                            _List_MD_Struttura.Add(_MD_Struttura);
                        }
                    }
                }

                else { throw new Exception("Nessuna corrispondenza nel file XML per un SQL da eseguire. Verificare XML_SQL nel progetto "); }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            return _List_MD_Struttura;

        }

    }

    /// <summary>
    /// Oggetto che contiene le varie configurazioni SQL per poter effettuare le operazioni del caso
    /// </summary>
    class MD_Struttura
    {

        public System.String RELATIONSHIP { get; set; }

        public Boolean ENABLING { get; set; }

        public Boolean ENABLING_UPDATE { get; set; }

        public System.String STORAGE_AREA { get; set; }

        public System.Int16 STORAGE_AREA_INTERNAL_FOLDER { get; set; }

        public System.Int32 MAX_FILES_IN_FOLDER { get; set; }

        public System.String INTEGRAZIONE_FOLDER { get; set; }

        public System.String ITEM_QUERY { get; set; }

    }
}
