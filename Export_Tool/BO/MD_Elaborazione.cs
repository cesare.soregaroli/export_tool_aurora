﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MD.Errore;
using Oracle.DataAccess.Client;
using MD.Configuration;
using System.Globalization;
using System.IO;
using MD.XML;

namespace MD.Elaborazione
{
    class MD_Elaborazione
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        public MD_Errori errore { get; set; }

        public void Gestione_SQL_Export_Tool(MD_Struttura _MD_Struttura)
        {
            OracleConnection cn = null;
            OracleCommand cmd = null;
            List<MD_profile> _list_MD_Profile = new List<MD_profile> { };
            MD_profile _MD_profile = null;
            try
            {

                cn = new OracleConnection(EXP_TOOL_Configuration.EXPORT_TOOL_CONNECTION_DATABASE);
                cmd = new OracleCommand(_MD_Struttura.ITEM_QUERY, cn);
                cmd.CommandType = System.Data.CommandType.Text;

                try
                {
                    cn.Open();
                }
                catch (Exception ex)
                {
                    errore.Errore = true;
                    errore.Errore_List.Add(string.Concat(DateTime.Now.ToString("yyyyMMdd_HHmmss"), " : ERRORE IN FASE DI Gestione_SQL_Export_Tool- ERRORE IN FASE DI CONNESSIONE AL DATABASE DOCSAMD: " + ex.Message + "\r\n"));
                    log.Error("ERRORE IN FASE DI CONNESSIONE AL DATABASE DOCSADM: " + ex.Message + "\r\n");
                    throw;
                }

                if (cn.State == System.Data.ConnectionState.Open)
                {
                    OracleDataReader reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {

                        while (reader.Read())
                        {
                            _MD_profile = new MD_profile();

                            _MD_profile.DOCNUMBER = string.IsNullOrEmpty(reader["DOCNUMBER"].ToString()) ? 0 : Convert.ToInt64(reader["DOCNUMBER"].ToString());
                            _MD_profile.FULL_FILENAME = string.IsNullOrEmpty(reader["FULL_FILENAME"].ToString()) ? String.Empty : reader["FULL_FILENAME"].ToString();
                            _MD_profile.ID_LAVORAZIONE = string.IsNullOrEmpty(reader["ID_LAVORAZIONE"].ToString()) ? String.Empty : reader["ID_LAVORAZIONE"].ToString();
                            _MD_profile.COD_NDG = string.IsNullOrEmpty(reader["COD_NDG"].ToString()) ? String.Empty : reader["COD_NDG"].ToString();
                            _MD_profile.TIPO_DOC_ID = string.IsNullOrEmpty(reader["TIPO_DOC_ID"].ToString()) ? String.Empty : reader["TIPO_DOC_ID"].ToString();
                            _MD_profile.COD_SPORT = string.IsNullOrEmpty(reader["COD_SPORT"].ToString()) ? String.Empty : reader["COD_SPORT"].ToString();
                            _MD_profile.COD_PROG = string.IsNullOrEmpty(reader["COD_PROG"].ToString()) ? String.Empty : reader["COD_PROG"].ToString();
                            _MD_profile.COD_CC = string.IsNullOrEmpty(reader["COD_CC"].ToString()) ? String.Empty : reader["COD_CC"].ToString();
                            _MD_profile.DATA_RICEZIONE = string.IsNullOrEmpty(reader["DATA_RICEZIONE"].ToString()) ? String.Empty : reader["DATA_RICEZIONE"].ToString();
                            _MD_profile.DATA_DOCUMENTO = string.IsNullOrEmpty(reader["DATA_DOCUMENTO"].ToString()) ? String.Empty : reader["DATA_DOCUMENTO"].ToString();
                            _MD_profile.CHANNEL_ID = string.IsNullOrEmpty(reader["CHANNEL_ID"].ToString()) ? String.Empty : reader["CHANNEL_ID"].ToString();

                            _list_MD_Profile.Add(_MD_profile);
                        }
                        if (_list_MD_Profile.Count > 0) { Genera_LavorazioneBatck(_list_MD_Profile, _MD_Struttura); }
                        else { log.Info("Nessun Record da lavorare"); }
                    }
                    else { log.Info("Nessun Record da lavorare"); }
                }
            }
            catch (Exception ex)
            {
                errore.Errore = true;
                errore.Errore_List.Add(string.Concat(DateTime.Now.ToString("yyyyMMdd_HHmmss"), " : ERRORE IN FASE DI Esecuzione della QUERY  : ", _MD_Struttura.ITEM_QUERY, " ", ex.Message + "\r\n"));

                log.Error($"ERRORE IN FASE DI Esecuzione della {_MD_Struttura.ITEM_QUERY}: " + ex.Message + "\r\n");
                log.Error(ex.StackTrace);
            }
            finally
            {
                if (cn != null && cn.State == System.Data.ConnectionState.Open)
                {
                    cn.Close();
                }
            }
        }

        private void Genera_LavorazioneBatck(List<MD_profile> _list_MD_Profile, MD_Struttura _MD_Struttura)
        {
            try
            {
                foreach (MD_profile _MD_profile in _list_MD_Profile)
                {
                    log.Info("====================");
                    log.Info($"INIZIO Lavorazione DOCNUMBER: {_MD_profile.DOCNUMBER} TIPO_DOC_ID: {_MD_profile.TIPO_DOC_ID} NDG: {_MD_profile.COD_NDG}");
                    try
                    {
                        var _nomefile = Genera_NomeFile(_MD_profile);
                        log.Info($"Funzione Genera_NomeFile: {_nomefile.ToString()}");
                        Genera_CopiaFile(_MD_profile, _nomefile,_MD_Struttura);
                    }
                    catch
                    {
                        log.Error($"Errore in Genera_LavorazioneBatck DOCNUMBER: {_MD_profile.DOCNUMBER}");
                    }
                    log.Info($"FINE Lavorazione DOCNUMBER: {_MD_profile.DOCNUMBER} ");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // o NDG(blank se nullo)
        //o SPORTELLO(blank se nullo)
        //o TIPO CONTO(blank se nullo)
        //o NUMERO CONTO(blank se nullo)
        //o TIPO DOCUMENTO(blank se nullo)
        //o DATA RICEZIONE(YYYYMMDD) (o blank se nulla o minore dell’anno 1901)
        //o DATA SCADENZA(YYYYMMDD) (o blank se nulla o minore dell’anno 1901)
        //o DOCNUMBER(aggiunto però in questo modo %DOCN1292780%)
        //-	Esempio: 76573-580-00-160854-RICO-20200316--%DOCN1292780%.PDF
        private System.String Genera_NomeFile(MD_profile _MD_Profile)
        {
            StringBuilder _nomeFile = new StringBuilder();
            System.String _output = string.Empty;
            try
            {
                _nomeFile.Append(String.IsNullOrEmpty(_MD_Profile.ID_LAVORAZIONE.Trim()) ? String.Empty : _MD_Profile.ID_LAVORAZIONE.Trim().Replace("-", "_"));
                //NDG(blank se nullo)
                _nomeFile.Append("-");
                _nomeFile.Append(String.IsNullOrEmpty(_MD_Profile.COD_NDG.Trim()) ? String.Empty : _MD_Profile.COD_NDG.Trim().Replace("-", "_"));
                //o SPORTELLO(blank se nullo)
                _nomeFile.Append("-");
                _nomeFile.Append(String.IsNullOrEmpty(_MD_Profile.COD_SPORT.Trim()) ? String.Empty : _MD_Profile.COD_SPORT.Trim().Replace("-", "_"));
                //o TIPO CONTO(blank se nullo)
                _nomeFile.Append("-");
                _nomeFile.Append(String.IsNullOrEmpty(_MD_Profile.COD_PROG.Trim()) ? String.Empty : _MD_Profile.COD_PROG.Trim().Replace("-", "_"));
                //o NUMERO CONTO(blank se nullo)
                _nomeFile.Append("-");
                _nomeFile.Append(String.IsNullOrEmpty(_MD_Profile.COD_CC.Trim()) ? String.Empty : _MD_Profile.COD_CC.Trim().Replace("-", "_"));
                //o TIPO DOCUMENTO(blank se nullo)
                _nomeFile.Append("-");
                _nomeFile.Append(String.IsNullOrEmpty(_MD_Profile.TIPO_DOC_ID.Trim()) ? String.Empty : _MD_Profile.TIPO_DOC_ID.Trim().Replace("-", "_"));
                //o DATA RICEZIONE(YYYYMMDD) (o blank se nulla o minore dell’anno 1901)
                _nomeFile.Append("-");
                if (String.IsNullOrEmpty(_MD_Profile.DATA_RICEZIONE.Trim()) ||
                    _MD_Profile.DATA_RICEZIONE.Equals("01/01/1753") ||
                    _MD_Profile.DATA_RICEZIONE.Equals("01/01/1901"))
                {
                    _nomeFile.Append(String.Empty);
                }
                else
                {
                    var dataFormattata = Convert.ToDateTime(_MD_Profile.DATA_RICEZIONE);
                    if (!dataFormattata.Equals(new DateTime(1900, 1, 1)) && !dataFormattata.Equals(new DateTime(1753, 1, 1)))
                    { _nomeFile.Append(dataFormattata.ToString("yyyyMMdd")); }
                    else { _nomeFile.Append(String.Empty); }
                }
                ;

                //o DATA SCADENZA(YYYYMMDD) (o blank se nulla o minore dell’anno 1901)
                _nomeFile.Append("-");
                if (String.IsNullOrEmpty(_MD_Profile.DATA_DOCUMENTO.Trim()) ||
                    _MD_Profile.DATA_DOCUMENTO.Equals("01/01/1753") ||
                    _MD_Profile.DATA_DOCUMENTO.Equals("01/01/1901"))
                {
                    _nomeFile.Append(String.Empty);
                }
                else
                {
                    var dataFormattata = Convert.ToDateTime(_MD_Profile.DATA_DOCUMENTO);
                    if (!dataFormattata.Equals(new DateTime(1900, 1, 1)) && !dataFormattata.Equals(new DateTime(1753, 1, 1)))
                    { _nomeFile.Append(dataFormattata.ToString("yyyyMMdd")); }
                    else { _nomeFile.Append(String.Empty); }
                }
                //modifica effettuata come da richiesta di Morandi per aggiunta channel id
                _nomeFile.Append("-");
                _nomeFile.Append(String.IsNullOrEmpty(_MD_Profile.CHANNEL_ID.Trim()) ? String.Empty : _MD_Profile.CHANNEL_ID.Trim().Replace("-", "_"));

                _nomeFile.Append("-");
                //o DOCNUMBER(aggiunto però in questo modo %DOCN1292780%)
                _nomeFile.Append(String.IsNullOrEmpty(_MD_Profile.DOCNUMBER.ToString()) ? String.Empty : $"%DOCN{ _MD_Profile.DOCNUMBER.ToString()}%");
                _nomeFile.Append(".pdf");
                _output = _nomeFile.ToString();
            }
            catch (Exception ex)
            {
                errore.Errore = true;
                errore.Errore_List.Add(string.Concat(DateTime.Now.ToString("yyyyMMdd_HHmmss"), " : ERRORE IN FASE DI Generazione del Nome del file per DOCNUMBER:", _MD_Profile.DOCNUMBER, " EXCP :", ex.Message + "\r\n"));

                log.Error($"ERRORE IN FASE DI Generazione del Nome del file per DOCNUMBER:", _MD_Profile.DOCNUMBER, " EXCP :", ex.Message);
                log.Error(ex.StackTrace);
                throw;
            }
            return _output;
        }

        private void Genera_CopiaFile(MD_profile _MD_Profile, System.String _nomeFile, MD_Struttura _MD_Struttura)
        {
            try
            {
                if (!File.Exists(_MD_Profile.FULL_FILENAME))
                {
                    errore.Errore = true;
                    errore.Errore_List.Add($"File non trovato nel path {_MD_Profile.FULL_FILENAME}");
                    throw new Exception($"File da copiare non presente nella folder {_MD_Profile.FULL_FILENAME}");
                }
                try
                {
                    //verifico se nella cartella ci sono piu di un tot numero di Files e se la cartella esiste
                    VerifySubfolder(_MD_Struttura);
                     var _pathDestinazione = Path.Combine(_MD_Struttura.STORAGE_AREA,string.Concat(_MD_Struttura.INTEGRAZIONE_FOLDER,_MD_Struttura.STORAGE_AREA_INTERNAL_FOLDER.ToString()), _nomeFile);
                    File.Copy(_MD_Profile.FULL_FILENAME, _pathDestinazione,true);
                    if (!File.Exists(_pathDestinazione))
                    {
                        throw new IOException($"File con DOCNUMBER : {_MD_Profile.DOCNUMBER} al PATH : {_MD_Profile.FULL_FILENAME} non presente nella Cartella di Destinazione");
                    }

                    log.Info($"File di partenza : {_MD_Profile.FULL_FILENAME}");
                    log.Info($"File di destinazione : {_pathDestinazione}");
                    if (_MD_Struttura.ENABLING_UPDATE)
                    {
                        //Effettuo il salvataggio dei dati
                        Gestione_SalvataggioDati(_MD_Profile);
                    }

                }
                catch (Exception ex)
                {
                    errore.Errore = true;
                    errore.Errore_List.Add(string.Concat(DateTime.Now.ToString("yyyyMMdd_HHmmss"),$" : File con DOCNUMBER : {_MD_Profile.DOCNUMBER} al PATH : {_MD_Profile.FULL_FILENAME} non copiato correttamente EXCP :", ex.Message + "\r\n"));

                    throw new Exception($"File con DOCNUMBER : {_MD_Profile.DOCNUMBER} al PATH : {_MD_Profile.FULL_FILENAME} non copiato correttamente");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void VerifySubfolder(MD_Struttura _MD_Struttura) {
            try
            {
                var _pathDestinazione = Path.Combine(_MD_Struttura.STORAGE_AREA,string.Concat(_MD_Struttura.INTEGRAZIONE_FOLDER, _MD_Struttura.STORAGE_AREA_INTERNAL_FOLDER.ToString()));
                //Verifico l'esistenza della cartella
                if (!Directory.Exists(_pathDestinazione)) { Directory.CreateDirectory(_pathDestinazione);}
                //verifico che il numero di Files contenuto nella medesima sia inferiore o uguale al numero inserito nel file di configurazione
                DirectoryInfo _dirInfo = new DirectoryInfo(_pathDestinazione);
                //estraggo il numero di file contenuti
                var numFiles = _dirInfo.GetFiles().Length;
                if (numFiles > _MD_Struttura.MAX_FILES_IN_FOLDER) {
                    _MD_Struttura.STORAGE_AREA_INTERNAL_FOLDER += 1;
                    var _pathDestinazionenew = Path.Combine(_MD_Struttura.STORAGE_AREA,String.Concat(_MD_Struttura.INTEGRAZIONE_FOLDER, _MD_Struttura.STORAGE_AREA_INTERNAL_FOLDER.ToString()));
                    if (!Directory.Exists(_pathDestinazionenew)) { Directory.CreateDirectory(_pathDestinazionenew); }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void Gestione_SalvataggioDati(MD_profile _MD_Profile)
        {
            OracleConnection cn = null;
            OracleCommand cmd = null;
            List<MD_profile> _list_MD_Profile = new List<MD_profile> { };
            try
            {
                cn = new OracleConnection(EXP_TOOL_Configuration.EXPORT_TOOL_CONNECTION_DATABASE);
                cmd = new OracleCommand("UPDATE PROFILE SET STATO_INDEX = :STATO_INDEX , DATA_PRIMO_EXPORT = :DATA_PRIMO_EXPORT WHERE DOCNUMBER = :DOCNUMBER ", cn);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.Add(new OracleParameter("STATO_INDEX", OracleDbType.NVarchar2, System.Data.ParameterDirection.Input));
                cmd.Parameters["STATO_INDEX"].Value = "INDEX";
                cmd.Parameters.Add(new OracleParameter("DATA_PRIMO_EXPORT", OracleDbType.Date, System.Data.ParameterDirection.Input));
                cmd.Parameters["DATA_PRIMO_EXPORT"].Value = System.DateTime.Now;
                cmd.Parameters.Add(new OracleParameter("DOCNUMBER", OracleDbType.Int64, System.Data.ParameterDirection.Input));
                cmd.Parameters["DOCNUMBER"].Value = _MD_Profile.DOCNUMBER;
                //cmd.Parameters["DOCNUMBER"].Value = 18064478;                
                try
                {
                    cn.Open();
                }
                catch (Exception ex)
                {
                    errore.Errore = true;
                    errore.Errore_List.Add(string.Concat(DateTime.Now.ToString("yyyyMMdd_HHmmss"), " : ERRORE IN FASE DI Gestione_SalvataggioDati- ERRORE IN FASE DI CONNESSIONE AL DATABASE DOCSAMD: " + ex.Message + "\r\n"));
                    log.Error("ERRORE IN FASE DI CONNESSIONE AL DATABASE DOCSADM: " + ex.Message + "\r\n");
                    throw;
                }

                if (cn.State == System.Data.ConnectionState.Open)
                {

                    var _idRet = cmd.ExecuteNonQuery();

                    if (_idRet < 1)
                    {
                        throw new Exception($"Update STATO_INDEX Non corretta per il DOCNUMBER {_MD_Profile.DOCNUMBER}");
                        //Da vedere se cancellare il file
                    }
                    else
                    {
                        log.Info($"Update STATO_INDEX effettuato correttamente per il DOCNUMBER : {_MD_Profile.DOCNUMBER}");
                    }

                }
            }
            catch (Exception ex)
            {
                errore.Errore = true;
                errore.Errore_List.Add(string.Concat(DateTime.Now.ToString("yyyyMMdd_HHmmss"), " : ERRORE IN FASE DI Esecuzione della UPDATE SUL DOCNUMBER: ", _MD_Profile.DOCNUMBER, " ", ex.Message + "\r\n"));

                log.Error($"ERRORE IN FASE DI UPDATE per il DOCNUMBER NUMERO:{_MD_Profile.DOCNUMBER} " + ex.Message + "\r\n");
                log.Error(ex.StackTrace);
            }
            finally
            {
                if (cn != null && cn.State == System.Data.ConnectionState.Open)
                {
                    cn.Close();
                }
            }

        }

        protected class MD_profile
        {
            public System.Int64 DOCNUMBER { get; set; }
            public System.String ID_LAVORAZIONE { get; set; }
            public System.String FULL_FILENAME { get; set; }
            public System.String COD_NDG { get; set; }
            public System.String TIPO_DOC_ID { get; set; }
            public System.String COD_SPORT { get; set; }
            public System.String COD_PROG { get; set; }
            public System.String COD_CC { get; set; }
            public System.String DATA_RICEZIONE { get; set; }
            public System.String DATA_DOCUMENTO { get; set; }
            public System.String CHANNEL_ID { get; set; }

        }
    }
}
