﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MD.Configuration;
using MD.Errore;
using MD.XML;
using MD.Mail;
using System.IO;
using MD.Elaborazione;


namespace MD
{
    class Program
    {

        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        private static MD_Errori _MD_Errori = new MD_Errori() { Errore_List = new List<string> { }, Errore = false };
        static void Main(string[] args)
        {
            try
            {
                log.Info("Begin - EXPORT TOOL AURORA");
                elaborazione_Export_Tool();
            }
            catch (Exception ex)
            {
                log.Error(ex, ex.Message);
                if (ex.InnerException != null) { log.Error(ex, ex.InnerException.StackTrace); }
                _MD_Errori.Errore = true;
                _MD_Errori.Errore_List.Add(ex.Message);
                log.Error(ex, ex.StackTrace);
            }
            finally
            {
                //verifico se vi sono degli errori gestiti all'interno dell'applicazione
                if (_MD_Errori.Errore)
                {
                    MD_Mail _MD_Mail = new MD_Mail();
                    System.String _log_File_Error = String.Empty;
                    _log_File_Error = _MD_Mail.CreateLogfile(_MD_Errori.Errore_List);
                    log.Info(_log_File_Error);
                    List<System.String> _errorFile = null;
                    if (File.Exists(_log_File_Error)) { _errorFile = new List<string> { _log_File_Error }; }
                    SendMail(_errorFile);
                    log.Info($"Cancellazione file di errore nella cartella dei Log");
                }
                log.Info("End - EXPORT TOOL AURORA");
                createMailLogFile();
            }
        }
        private static void elaborazione_Export_Tool()
        {
            List<MD_Struttura> _List_MD_Struttura = null;
            try
            {
                EXP_TOOL_Configuration.VerifyPrincipalFolder();

                var _xml = new MD_XML();
                _List_MD_Struttura = _xml.Extract_SqlToXml();

                if (_List_MD_Struttura != null && _List_MD_Struttura.Count > 0)
                {
                    var _md_elaborazione = new MD_Elaborazione();
                    _md_elaborazione.errore = _MD_Errori;
                    foreach (MD_Struttura _MD_Struttura in _List_MD_Struttura)
                    {
                        EXP_TOOL_Configuration.VerifyPrincipalFolderToXML(_MD_Struttura);
                        log.Info($" Inizio Esecuzione Tipologia { _MD_Struttura.RELATIONSHIP }");
                        if (_MD_Struttura.ENABLING)
                        {
                            log.Info($" Elaborazione Abilitata");
                            _md_elaborazione.Gestione_SQL_Export_Tool(_MD_Struttura);
                        }
                        else { log.Info($" Elaborazione NON Abilitata"); }
                        log.Info($" Fine Esecuzione Tipologia { _MD_Struttura.RELATIONSHIP }");
                    }
                    _MD_Errori = _md_elaborazione.errore;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        static void createMailLogFile()
        {
            System.String _cartellaLog = System.DateTime.Now.ToString("yyyy-MM-dd");
            System.String _pathCartella = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log", _cartellaLog);
            List<System.String> _listFile = new List<string> { };
            try
            {
                log.Info($"Cartella Dei Log {_pathCartella}");
                NLog.LogManager.Shutdown();
                if (Directory.GetFiles(_pathCartella).Length > 0)
                {
                    _listFile = Directory.GetFiles(_pathCartella, "*", SearchOption.TopDirectoryOnly).ToList();
                }
                InviaMailLog(_listFile);
            }
            catch (Exception)
            {
                throw;
            }

        }

        private static void InviaMailLog(List<System.String> _LogFile)
        {
            MD_Mail _Crif_Mail = new MD_Mail();
            try
            {

                _Crif_Mail.email_from = System.Configuration.ConfigurationManager.AppSettings["MailFrom_Log"].ToString();
                _Crif_Mail.email_to = System.Configuration.ConfigurationManager.AppSettings["MailTo_Log"].ToString();
                _Crif_Mail.email_sendemail = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["Abilitazione_Mail_Log"].ToString());
                _Crif_Mail.email_smtpserver = System.Configuration.ConfigurationManager.AppSettings["MailSmtp_Log"].ToString();
                _Crif_Mail.email_Attachement = _LogFile;

                _Crif_Mail.SendMailLog();

            }
            catch (Exception)
            {
                throw;
            }
        }

        #region "SendMail"        
        /// <summary>
        /// Funzione invio Mail di errore 
        /// </summary>
        /// <param name="ex">Eccezione</param>
        private static void SendMail(List<System.String> _errorFile)
        {
            MD_Mail _Crif_Mail = new MD_Mail();
            try
            {
                log.Info("Start => SendMail");
                _Crif_Mail.email_from = System.Configuration.ConfigurationManager.AppSettings["email_from"].ToString();
                _Crif_Mail.email_to = System.Configuration.ConfigurationManager.AppSettings["email_to"].ToString();
                _Crif_Mail.email_cc = System.Configuration.ConfigurationManager.AppSettings["email_cc"].ToString();
                _Crif_Mail.email_sendemail = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["email_sendemail"].ToString());
                _Crif_Mail.email_smtpserver = System.Configuration.ConfigurationManager.AppSettings["email_smtpserver"].ToString();
                _Crif_Mail.email_Attachement = _errorFile;
                //invio mail a Microframe
                _Crif_Mail.SendMailError(true);
                log.Info("End => SendMail");
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }

}
